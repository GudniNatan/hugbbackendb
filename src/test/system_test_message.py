import unittest
import asyncio
import json
from app.client import client

class TestSystemMessage(unittest.TestCase):
    def setUp(self):
        dummy_message = {
            "sender_id": -1,
            "receiver_id": -2,
            "timestamp": "09/28/2019 13:00:00",
            "message": "That relatable moment when :|"

        }
        body = {
            "op": "send_message",
            "message_model": dummy_message
        }
        result = asyncio.get_event_loop() \
                        .run_until_complete(client.send_test_message(body))
        self.__dummy_message = json.loads(result)
    
    def tearDown(self):
        body = { "op": "hard_delete_message", "message_id": int(self.__dummy_message["id"]) }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = { "op": "decrement_next_message_id" }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
    
    """ Tests if send_message method returns correct message model. """
    def test_send_message_returns_message_model(self):
        self.assertEqual(type(self.__dummy_message), dict)
        self.assertTrue("id" in self.__dummy_message)
        self.assertTrue("sender_id" in self.__dummy_message)
        self.assertTrue("receiver_id" in self.__dummy_message)
        self.assertTrue("timestamp" in self.__dummy_message)
        self.assertTrue("message" in self.__dummy_message)
    
    """ Tests if get_message_info returns correct info. """
    def test_get_message_info_returns_correct_message(self):
        # Get info from the message
        body = {
            "op": "get_message_info",
            "message_id": self.__dummy_message["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        message_info = json.loads(return_val)

        # Check if info matches
        self.assertEqual(message_info["id"], self.__dummy_message["id"])
        self.assertEqual(message_info["sender_id"], self.__dummy_message["sender_id"])
        self.assertEqual(message_info["receiver_id"], self.__dummy_message["receiver_id"])
        self.assertEqual(message_info["timestamp"], self.__dummy_message["timestamp"])
        self.assertEqual(message_info["message"], self.__dummy_message["message"])
    
    """ Tests if get_message_to method returns correct message. """
    def test_get_message_to_returns_correct_message(self):
        # Get info from the message
        body = {
            "op": "get_messages_to",
            "user_id": self.__dummy_message["receiver_id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        messages = json.loads(return_val)

        # Check if info matches
        for message_info in messages.values():
            self.assertEqual(message_info["receiver_id"], self.__dummy_message["receiver_id"])
    
    """ Tests if get_message_by returns correct message. """
    def test_get_message_by_returns_correct_message(self):
        # Get info from the message
        body = {
            "op": "get_messages_by",
            "user_id": self.__dummy_message["sender_id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        messages = json.loads(return_val)

        # Check if info matches
        for message_info in messages.values():
            self.assertEqual(message_info["sender_id"], self.__dummy_message["sender_id"])
    
    """ Tests if get_message_between method returns correct message. """
    def test_get_message_between_returns_correct_message(self):
        # Get info from the message
        body = {
            "op": "get_messages_between",
            "userid1": self.__dummy_message["receiver_id"],
            "userid2": self.__dummy_message["sender_id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        messages = json.loads(return_val)

        # Check if info matches
        for message_info in messages.values():
            self.assertEqual(message_info["receiver_id"], self.__dummy_message["receiver_id"])
            self.assertEqual(message_info["sender_id"], self.__dummy_message["sender_id"])

    """ Tests if get_all_messages method does not return empty dictionary. """
    def test_get_all_messages_returns_non_empty_dict(self):
        body = { "op": "get_all_messages" }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        all_messages = json.loads(return_val)
        self.assertTrue(type(all_messages) == dict)
        self.assertTrue(len(all_messages) > 0)

if __name__ == "__main__":
    unittest.main()
