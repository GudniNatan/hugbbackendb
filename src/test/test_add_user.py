from app.services import user_service
import unittest


""" Tests the add_user() function """
class TestAddUser(unittest.TestCase):
    """ Sets up the test for each test method """
    def setUp(self):
        self.__user_service = user_service.UserService()
        self.User4 = {"name": "John Johnsson", "email": "jon@jonsson.com", "username": "jonsson","password": "yomama", "is_admin": False}
        self.User5 = {"name": "John Johnsson", "email": "yo@man.com", "username": "jonsson","password": "yomama", "is_admin": False}
        self.userdata = self.__user_service.get_all_users()
        self.lenb4add = len(self.userdata)
        self.__added_user = self.__user_service.add_user(self.User4)

    """ Cleans up for the next test method """
    def tearDown(self):
        self.__user_service.hard_delete_user(self.__added_user["id"])
        self.__user_service.decrement_next_user_id()

    """ Tests adding a valid user """
    def test_add_user_success(self):
        self.assertTrue(self.__added_user["id"])
        self.assertTrue(self.__added_user["status"])
        self.assertEqual(self.__added_user["name"], self.User4["name"])
        self.assertEqual(self.__added_user["email"], self.User4["email"])
        self.assertEqual(self.__added_user["username"], self.User4["username"])
        self.assertEqual(self.__added_user["password"], self.User4["password"])
        self.assertEqual(self.__added_user["is_admin"], self.User4["is_admin"])
        
    """ Tests if the given id of a new user is correct """
    def test_correct_id_assigned(self):
        self.assertEqual(self.__added_user["id"], self.__user_service.get_next_user_id()-1)

    """ Tests adding a user when given an invalid email """
    def test_add_user_invalid_email(self):
        newuser = {"name": "iamevil", "email": "someinvalidemail", "username": "evil", "password": "actuallygood", "isadmin": "False"}
        with self.assertRaises(ValueError):
            self.__user_service.add_user(newuser)

    """ Tests adding a user that has the same email as an already existing user """
    def test_add_user_email_exists(self):
        with self.assertRaises(ValueError):
            self.__user_service.add_user(self.User4)

    """ Tests adding a user that has a missing attribute """
    def test_add_user_missing_attribute(self):
        with self.assertRaises(ValueError):
            self.__user_service.add_user(self.__create_bad_User4())
    
    """ Helper function that creates a invalid user """
    def __create_bad_User4(self):
        new_user = {"name": "John Cena", "email": "Cant@me.com", "username": None, "password": "Invisible", "isadmin": False}
        return new_user
        
if __name__ == "__main__":
       unittest.main()