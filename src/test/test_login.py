import unittest
from app.utility.session_id_maker import SessionIdMaker
from app.services.user_service import UserService
from app.repositories.session_repository import SessionRepository
from app.repositories.user_repository import UserRepository

""" Tests the backend's login functionality """
class TestChat(unittest.TestCase):
    """ Routine program run before each test """
    def setUp(self):
        self.__USER_ID = 1
        self.__session_id_maker = SessionIdMaker()
        self.__user_service = UserService()
        self.__session_repo = SessionRepository()
        self.__user_repository = UserRepository()
        self.__user_repository.add_user({
            "id": self.__USER_ID,
            "name": "Don't delete me 1",
            "email": "admin@admin.com",
            "username": "admin",
            "password": "admin",
            "is_admin": False
        })
        
    
    def tearDown(self):
        self.__user_service.hard_delete_user(self.__USER_ID)

    """ Test if SessionIdMaker returns a string """
    def test_session_id_maker_return_a_string(self):
        session_id = self.__session_id_maker.create_session_id()
        self.assertEqual(type(session_id), str)
    
    """ Test if SessionIdMaker returns unique strings """
    def test_session_id_maker_return_unique_strings(self):
        session_id_array = []
        session_id_set = set()
        for _ in range(1000):
            session_id = self.__session_id_maker.create_session_id()             
            session_id_array.append(session_id)
            session_id_set.add(session_id)
        self.assertEqual(len(session_id_array), len(session_id_set))
    
    """ Test if user_log_in returns a string (session ID) """
    def test_log_in_user_return_session_string(self):
        session_id = self.__user_service.log_in_user(self.__USER_ID)
        self.assertEqual(type(session_id), str)
    
    """  Test if user_log_in updates the session ID list, given a valid user id    """
    def test_log_in_user_update_session_ids(self):
        all_session_ids = self.__session_repo.get_all_session_ids()
        user_session_id_before = all_session_ids[str(self.__USER_ID)]
        user_session_id_after = self.__user_service.log_in_user(self.__USER_ID)
        self.assertNotEqual(user_session_id_before, user_session_id_after)
    
    """ Test if is_logged_in returns False if user is not logged in """
    def test_is_logged_in_return_false_when_logged_out(self):
        is_logged_in = self.__user_service.is_logged_in(self.__USER_ID, "")
        self.assertFalse(is_logged_in)

    """ Test if get_user_id_by_email_password works returns the correct user """
    def test_get_user_id_by_email_password_get_correct_id(self):
        email = "admin@admin.com"
        password = "admin"
        user_id = \
        self.__user_service.get_user_id_by_email_password(email, password)
        self.assertEqual(user_id, 1)

    """ Test if get_userid_by_username_password works returns None if a user is not found """
    def test_get_user_id_by_email_password_return_None(self):
        email = "wha@wha.com"
        password = "wha"
        user_id = \
        self.__user_service.get_user_id_by_email_password(email, password)
        self.assertIsNone(user_id)