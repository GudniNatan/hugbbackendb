from app.repositories.user_repository import UserRepository
from app.repositories.session_repository import SessionRepository
from app.utility.session_id_maker import SessionIdMaker

"""
The service class for users. Checks the input the user gives and calls the Repository layer to
recieve data or change it.
"""
class UserService(object):
    """ Initializes the class with instances of the classes UserRepository and SessionIdMaker """
    def __init__(self):
        self.__user_repository = UserRepository()
        self.__session_repository = SessionRepository()
        self.__session_id_maker = SessionIdMaker()

    """ Logs in the specified user """
    def log_in_user(self, user_id):
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Can't log in user with ID={user_id} since he/she does not exist")
        new_session_id = self.__session_id_maker.create_session_id()
        self.__session_repository.set_session_id(user_id, new_session_id)
        return new_session_id

    """ Checks if the email is valid or not """
    def check_valid_email(self, user_dict):
        if ("@" in user_dict["email"] and "." in user_dict["email"]):
            return True
        return False
    
    """ Checks if another user already has that email """
    def email_already_exists(self, user_dict):
        all_users = self.__user_repository.get_all_users()
        for curr_user in all_users.values():
            if (user_dict["email"] == curr_user["email"]):
                return True
        return False
    
    """ Checks if all attributes are present in the user object """
    def validate_attributes(self, userObj):
        if (userObj["email"] != None and userObj["username"] != None and userObj["password"] != None and userObj["name"] != None):
            return True
        return False

    """ Returns the specified user from the database """    
    def get_user(self, user_id):
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Cannot get info from user with ID={user_id} since he/she does not exist")
        return self.__user_repository.get_user(user_id)
    
    """ Returns all users in the database """
    def get_all_users(self):
        return self.__user_repository.get_all_users()
    
    """ Return all registered users that conform to certain search criteria """
    def filter_users(self, search_model):
        all_users = self.get_all_users()
        valid_users = {}
        for user in all_users.values():
            if  (not search_model["name"] or user["name"] == search_model["name"]) and \
                (not search_model["email"] or user["email"] == search_model["email"]) and \
                (not search_model["username"] or user["username"] == search_model["username"]):
                valid_users[str(user["id"])] = user
        return valid_users
    
    """ Adds the user to the database """
    def add_user(self, user_dict):
        self.__validate_user_dict(user_dict)
        if self.email_already_exists(user_dict):
            raise ValueError("Email '{}' already in use!".format(user_dict["email"]))
        elif self.check_valid_email(user_dict) == False:
            raise ValueError("Email '{}' is not a valid email".format(user_dict["email"]))
        user_dict["id"] = self.get_next_user_id()
        user_dict["status"] = 1
        return self.__user_repository.add_user(user_dict)
    
    """ Sets the specified user to removed in the database """
    def remove_user(self, user_id):
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Cannot remove user with ID={user_id} since he/she does not exist")
        user = self.__user_repository.get_user(user_id)
        if user["is_admin"]:
            raise ValueError(f"Cannot remove an admin from the system")
        self.__user_repository.remove_user(user_id)
    
    """ Makes a user that was formerly deleted available again """
    def recover_user(self, user_id):
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Cannot recover user with ID={user_id}, he/she does not exist")
        self.__user_repository.recover_user(user_id)
    
    """ Removes the specified user from the database """
    def hard_delete_user(self, user_id):
        if isinstance(user_id, int) == False:
            return "Invalid user ID for hard delete"
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Cannot hard delete a user with ID={user_id}, he/she doesn't exist")
        self.__user_repository.hard_delete_user(user_id)

    """ Get the user associated with a session id """
    def get_user_id_from_session(self, session_id):
        sessions = self.__session_repository.get_all_session_ids()
        for key, value in sessions.items():
            if value == session_id:
                return key
        raise ValueError(f"Session id '{session_id}' not found")

    
    """ Checks if the user is already in the data """
    def __check_if_user_in(self, user_id, input_dict):
        if str(user_id) in input_dict:
            return True
        return False
    
    """ Checks if the specified user is an admin or not """
    def __check_if_admin(self, user_id):
        user_is = self.__user_repository.get_user(user_id)
        if (user_is["isadmin"] == True):
            return True
        return False
    
    """ Checks the variable type of the id input """
    def __check_id_type(self, user_id):
        if type(user_id) != int:
            return True
        return False
    
    """ Returns the next avaliable ID """
    def get_next_user_id(self):
        maxid = self.__user_repository.get_next_user_id()
        return maxid

    """ Is the user currently logged in? """
    def is_logged_in(self, user_id, *args, **kwargs):
        sessions = self.__session_repository.get_all_session_ids()
        session = sessions.get(user_id, None)
        return session is not None
    
    """ Returns a user's id that has the specified email and password """
    def get_user_id_by_email_password(self, email, password):
        user_id = None
        all_users = self.__user_repository.get_all_users()
        for user in all_users.values():
            same_email = user["email"] == email
            same_password = user["password"] == password
            if same_email and same_password:
                user_id = user["id"]
                break
        return user_id
    """ Decrements next user id. (For testing purposes only)"""
    def decrement_next_user_id(self):
        self.__user_repository.decrement_next_user_id()

    """ Checks if a user dictionary is valid """
    def __validate_user_dict(self, user_dict):
        if user_dict == None:
            raise ValueError("Some required information was not given")
        has_valid_properties, message = self.__check_user_dict_properties(user_dict)
        if not has_valid_properties:
            raise ValueError(message)
        has_valid_types, message = self.__check_user_dict_value_types(user_dict)
        if not has_valid_types:
            raise ValueError(message)
    
    """ Checks if a user dictionary has only the valid properties """
    def __check_user_dict_properties(self, user_dict):
        properties = ["name", "email", "username", "password", "is_admin"]
        for prop in properties:
            if prop not in user_dict:
                return (False, "Model is missing property: {}".format(prop))
        if len(user_dict.keys()) != len(properties):
            return (False, "Model includes some unwanted properties")
        return (True, "All is well")
    
    """ Checks if a user dictionary has all the valid value types """
    def __check_user_dict_value_types(self, user_dict):
        if  type(user_dict["name"]) != str or \
            type(user_dict["email"]) != str or \
            type(user_dict["username"]) != str or \
            type(user_dict["password"]) != str or \
            type(user_dict["is_admin"]) != bool:
            return (False, "Some property/properties are not of the correct type")
        return (True, "All is well")