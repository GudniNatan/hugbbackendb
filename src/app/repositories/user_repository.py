import json
from pathlib import Path

"""
The repository class for users. Takes care of the database for users and has methods to return data
and change it.
"""
class UserRepository(object):
    def __init__(self):
        self.users = self.get_all_users()

    """ Returns all users in the database """
    def get_all_users(self):
        all_users = {}
        with open("app/data/userdata.json", "r") as filestream:
            all_users = json.loads(filestream.read())
        return all_users
    
    """ Returns a user that has the same id as the user_id input """
    def get_user(self, user_id):
        self.users = self.get_all_users()
        return self.users[str(user_id)]

    """ Returns the id that the next user added to the database will have """
    def get_next_user_id(self):
        next_user_id = 0
        with open("app/data/ids.json", "r") as filestream:
            ids = json.loads(filestream.read())
            next_user_id = ids["next_user_id"]
        return next_user_id

    """ Adds a user to the database """
    def add_user(self, user_dict):
        all_users = self.get_all_users()
        all_users[str(user_dict["id"])] = user_dict
        with open("app/data/userdata.json", "w") as filestream:
            filestream.write(json.dumps(all_users))
        self.__increment_next_user_id()
        return user_dict
    
    """ Set a user with the given ID to removed """
    def remove_user(self, user_id):
        all_users = self.get_all_users()
        all_users[str(user_id)]["status"] = 2
        with open("app/data/userdata.json", "w") as filestream:
            filestream.write(json.dumps(all_users))
    
    """ Permanently deletes a user from the system """
    def hard_delete_user(self, userid):
        all_users = {}
        with open("app/data/userdata.json", "r") as filestream:
            all_users = json.loads(filestream.read())
        del all_users[str(userid)]
        with open("app/data/userdata.json", "w") as filestream:
            filestream.write(json.dumps(all_users))
    
    """ Marks a user that was formerly deleted as 'available' """
    def recover_user(self, user_id):
        all_users = self.get_all_users()
        all_users[str(user_id)]["status"] = 1
        with open("app/data/userdata.json", "w") as filestream:
            filestream.write(json.dumps(all_users))

    """ Decrements next user ID. (For testing purpose only) """
    def decrement_next_user_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_user_id"] -= 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))
    
    """ Returns True if a user with the given user_id exists, False otherwise """
    def user_exists(self, user_id):
        with open("app/data/userdata.json", "r") as filestream:
            all_users = json.loads(filestream.read())
            return str(user_id) in all_users

    """ Used to find the path to our json file """ 
    def __get_path(self):
        base_path = Path(__file__).parent
        return (base_path / "../data/userdata.json").resolve()
    
    """ Increment next user ID """
    def __increment_next_user_id(self):
        ids = {}
        with open("app/data/ids.json", "r") as filestream:
            ids = json.loads(filestream.read())
        with open("app/data/ids.json", "w") as filestream:
            ids["next_user_id"] += 1
            filestream.write(json.dumps(ids))
