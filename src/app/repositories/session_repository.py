import json

class SessionRepository(object):
    """ Changes the session id that has the same id as the session_id input """
    def set_session_id(self, user_id, session_id):
        all_sessions = {}
        with open("app/data/sessiondata.json", "r") as filestream:
            all_sessions = json.loads(filestream.read())
        all_sessions[str(user_id)] = session_id
        with open("app/data/sessiondata.json", "w") as filestream:
            filestream.write(json.dumps(all_sessions))
    
    """ Returns all session ids in the database """
    def get_all_session_ids(self):
        all_session_ids = {}
        with open("app/data/sessiondata.json", "r") as filestream:
            all_session_ids = json.loads(filestream.read())
        return all_session_ids
    
    """ Returns a user's ID if session_id exists, otherwise None """
    def authenticate_user(self, session_id):
        if not session_id:
            return None
        authenticated_user_id = None
        all_session_ids = self.get_all_session_ids()
        for user_id in all_session_ids:
            if all_session_ids[user_id] == session_id:
                authenticated_user_id = user_id
                break
        return authenticated_user_id