# Server - message functions

Here you can find all of the different message related functionality that the file [server.py](../../../src/app/server.py) makes available to the client. Within each JSON body, you must include a `"op"` value in order to specify the operation you want the server to perform. All properties are required unless a property is specified to be optional.

## Table of Contents

1. [send_message](#send_message)
2. [get_messages_to](#get_messages_to)
3. [get_messages_by](#get_messages_by)
4. [get_messages_between](#get_messages_between)
5. [get_message_info](#get_message_info)
6. [get_all_messages](#get_all_messages)
7. [hard_delete_message](#hard_delete_message)
8. [decrement_next_message_id](#decrement_next_message_id)

---

## send_message

### Description

Creates and stores a new message object with the given values.

### Example JSON body

``` json
{
    "op": "send_message",
    "message_model": {
        "message": "... the bee should not be able to fly...",
        "sender_id": 2,
        "receiver_id": 3,
        "timestamp": "09/24/2019 16:00:00"
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| message_model | Object | Holds the information about the message to send.
| message     | String | The text that the message should contain
| sender_id   | Number | The ID of the user that sent the message
| receiver_id | Number | The ID of the user that will receive the message
| timestamp   | String | The date the message was sent on the format: MM/dd/yyyy hh:mm:ss

<br />

## send_message_usernames

### Description

Similar to `send_message` but uses usernames instead of user_ids.

### Example JSON body

``` json
{
    "op": "send_message_usernames",
    "message_model": {
        "message": "... the bee should not be able to fly...",
        "sender_username": "fake_news3",
        "receiver_username": "TheHillsAreAlive2009",
        "timestamp": "09/24/2019 16:00:00"
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| message_model | Object | Holds the information about the message to send.
| message     | String | The text that the message should contain
| sender_username   | String | The usenrmae of the user that sent the message
| receiver_username | String | The username of the user that will receive the message
| timestamp   | String | The date the message was sent on the format: MM/dd/yyyy hh:mm:ss

<br />


## get_messages_to

### Description

Returns all the messages that have been sent to a specific user.

### Example JSON body

``` json
{
    "op": "get_messages_to",
    "user_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user you want to get all messages sent to.

<br />

## get_messages_by

### Description

Returns all the messages that the user with the given ID has sent to others.

### Example JSON body

``` json
{
    "op": "get_messages_by",
    "user_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user you want to get all messages sent by.

<br />

## get_messages_between

### Description

Returns all the messages that have been sent between two users.

### Example JSON body

``` json
{
    "op": "get_messages_between",
    "userid1": 2,
    "userid2": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| userid1 | Number | The ID of one user who participated in a conversation.
| userid2 | Number | The ID of the other user who participated in the conversation.

<br />

## get_message_info

### Description

Gets and returns all information regarding a message with the specified ID.

### Example JSON body

``` json
{
    "op": "get_message_info",
    "message_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| message_id | Number | The ID of the message to get information about.

<br />

## get_all_messages

### Description

Returns all the messages that are registered within the system.

### Example JSON body

``` json
{
    "op": "get_all_messages"
}
```

### Property descriptions

There are no additional properties to describe.

<br />

## hard_delete_message

> **WARNING**: This method should only be used in test files, not in production.

### Description

Instead of just marking a message as deleted, this method *actually* deletes a message from the system, so be careful with it.

### Example JSON body

``` json
{
    "op": "hard_delete_message",
    "message_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| message_id | Number | The ID of the message to delete, forever.

## decrement_next_message_id

> **WARNING**: This method should only be used in test files, not in production.

### Description

Decrements the next available ID for messages, should only be used in test files.

### Example JSON body

``` json
{
    "op": "decrement_next_message_id"
}
```

### Property descriptions

There are no extra properties to describe.